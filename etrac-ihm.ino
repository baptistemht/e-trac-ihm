#include <LiquidCrystal_PCF8574.h>
#include <Wire.h>

#define BUZZER_PIN D12
#define LED_PIN LED_BUILTIN
#define BTN_OK_PIN D6
#define BAR_LENGTH 16
#define MODE_COUNT 2

LiquidCrystal_PCF8574 lcd(0x27);

enum State {LOADING, READY, ERROR_STATE};
enum Error {NO_DISPLAY, NO_CONNECT, VERY_LOW_BATTERY, UNKNOWN};
String Mode[] = {"LAPIN ", "TORTUE"};


byte leftBar[] = {0b11111, 0b10000, 0b10000, 0b10000, 0b10000, 0b10000, 0b10000, 0b11111};
byte centerBar[] = {0b11111, 0b00000, 0b00000, 0b00000, 0b00000, 0b00000, 0b00000, 0b11111};
byte rightBar[] = {0b11111, 0b00001, 0b00001, 0b00001, 0b00001, 0b00001, 0b00001, 0b11111};
byte halfBarItem[] = {0b11111, 0b11100, 0b11100, 0b11100, 0b11100, 0b11100, 0b11100, 0b11111};
byte fullBarItem[] = {0b11111, 0b11111, 0b11111, 0b11111, 0b11111, 0b11111, 0b11111, 0b11111};

byte inputDirection; //0: AV    1: AR
int inputBattery;
int inputCommand;
int inputMode;

State state = LOADING;

void setup(){
    Serial.begin(115200);

    pinMode(BUZZER_PIN, OUTPUT);
    pinMode(LED_PIN, OUTPUT);

    attachInterrupt(digitalPinToInterrupt(BTN_OK_PIN), interruptBtnOK, RISING);

    inputDirection = 1;
    inputBattery = 100;
    inputCommand = 0;
    inputMode = 0;

    if(initDisplay() != 0) setError(NO_DISPLAY);

    delay(1000);

    state = READY;
}

void loop(){

    switch (state){
    case LOADING:
        /* code */
        break;
    case READY:
        displayDirection(inputDirection);
        displayBattery(inputBattery);
        displayBar(inputCommand);
        displayMode(inputMode);

        //inputDirection^=1;
        //inputBattery--;
        inputCommand++;
        delay(400);
        
        digitalWrite(LED_PIN, HIGH);
        //tone(BUZZER_PIN, 433);
        delay(400);
        digitalWrite(LED_PIN, LOW);
        //noTone(BUZZER_PIN);
        //setError(VERY_LOW_BATTERY);
        break;
    case ERROR_STATE:
        digitalWrite(LED_PIN, HIGH);
        break;
    default:
        break;
    }

    delay(100);
    
}

void interruptBtnOK(){
    if(state == ERROR_STATE){
        state = READY;
        lcd.clear();
    }else if(state == READY){
        selectMode();
    }
}

void selectMode(){
    inputMode++;
    if(inputMode==MODE_COUNT) inputMode=0;
    //send command
}

uint8_t initDisplay(){
    Wire.begin();
    Wire.beginTransmission(0x27);
    uint8_t error = Wire.endTransmission();

    if(error != 0){
        Serial.print("LCD not found: ");
        Serial.println(error);
        return error;
    }

    Serial.println("LCD found.");

    lcd.begin(16, 2);
    lcd.setBacklight(255);
    lcd.home();
    lcd.clear();

    lcd.createChar(0, leftBar);
    lcd.createChar(1, centerBar);
    lcd.createChar(2, rightBar);
    lcd.createChar(3, fullBarItem);

    lcd.setCursor(2,0);
    lcd.print("E-Trac v1.0");

    return 0;
}

void setError(Error err){
    state = ERROR_STATE;
    lcd.clear();
    lcd.setCursor(5,0);
    lcd.print("Erreur");
    lcd.setCursor(0,1);
    switch (err){
    case NO_CONNECT:
        lcd.print("Pas de connexion.");
        break;
    case VERY_LOW_BATTERY:
        lcd.print("Batterie faible.");
        break;
    case UNKNOWN:
    default:
        lcd.print("Erreur inconnue.");
        break;
    }
    
}

void displayMode(int mode){
    lcd.setCursor(8,1);
    lcd.print(Mode[mode]);
}

void displayBar(int percent){
    //Fill the power bar on the screen. Between 0 and 100.
    if(percent < 0) percent = 0;
    if(percent > 100) percent = 100;
    int n = map(percent, 0, 100, 0, BAR_LENGTH);
    int i;

    lcd.setCursor(0,0);

    for(i = 0; i < BAR_LENGTH; i++){
        if(i >= n){
            if(i == 0){
                lcd.write(byte(0));
            }else if(i == BAR_LENGTH-1){
                lcd.write(byte(2));
            }else{
                lcd.write(byte(1));
            }
        }else{
            lcd.write(byte(3));
        }   
    }
}

void displayBattery(int battery){
    //Fill the battery level on screen. 0 - 100.
    if(battery > 100) battery = 100;
    if(battery < 0) battery = 0;

    lcd.setCursor(3,1);
    lcd.print(battery);

    if(battery == 100){
        lcd.setCursor(6, 1);
        lcd.print("%");
    }else if(battery >= 10){
        lcd.setCursor(5, 1);
        lcd.print("% ");
    }else{
        lcd.setCursor(4, 1);
        lcd.print("%  ");
    }
}

void displayDirection(byte direction){
    lcd.setCursor(0, 1);
    if(direction){
        lcd.print("AV");
    }else{
        lcd.print("AR");
    }
}